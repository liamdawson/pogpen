# Parameterised Operations Game Plan rENderer

Converts a list of parameters and a content document into an interactive playbook, to assist with documenting operations tasks.

See the `test_data` folder for sample input files.